# Go Guessing Game Experiment

## Overview

This project is a fun little experiment demonstrating the challenges of using global variables in concurrent programming with Go. The game involves worker goroutines that modify a global variable and player goroutines that attempt to guess its value. It highlights the pitfalls of global variables by showcasing how multiple goroutines can unpredictably change a shared value. To make it more interesting, players can use various "weapons" to hinder their opponents.

## How the Game Works

- **Workers**: Worker goroutines periodically change the global variable to a new random value within a specified range.
- **Players**: Player goroutines attempt to guess the value of the global variable. Each player has a limited number of guesses.
- **Weapons**: Players can use weapons to inhibit or delay other players, making the game more challenging and strategic.

## Weapons

- **Bomb**: Removes one or more players from being able to guess for a variable number of turns.
- **Expand**: Expands the guessing window of a player who is close to the correct answer, making it harder for them to guess correctly.
- **Delay Bomb**: Increases the delay between guesses for a player, slowing them down for a few turns.

## Configuration

You can configure various aspects of the game, including:

- **Number of workers**: The number of worker goroutines.
- **Number of players**: The number of player goroutines.
- **Max guesses per player**: The number of guesses each player can make.
- **Max number**: The range within which the global variable can be set.
- **Guess window**: The range within which a guess is considered close to the correct value.
- **Worker delay**: The delay between each worker's change of the global variable.
- **Player delay**: The delay between each player's guesses.

## How to Run the Game

1. **Clone the repository**:

   ```sh
   git clone <repository_url>
   cd <repository_directory>
   ```

2. **Run the game**:

   ```sh
   go run main.go
   ```

3. **Run tests**:
   ```sh
   go test -v
   ```

## Example Output

```plaintext
Worker 1 changed globalVar to 42
Player 0 guessed 56 which is within 10 of the correct value.
Player 1 guessed 15 and is inhibited for 2 more turns.
Player 2 guessed 84 and expanded the guess window to 30.
Player 3 used a bomb on Player 2.
Player 4 used a delay bomb on Player 0.
...
Game Over
Player 0 - Total Guesses: 10, Successful Guesses: 0, Times Inhibited: 1, Times Expanded: 0
Player 1 - Total Guesses: 10, Successful Guesses: 0, Times Inhibited: 0, Times Expanded: 1
Player 2 - Total Guesses: 10, Successful Guesses: 1, Times Inhibited: 1, Times Expanded: 1
Player 3 - Total Guesses: 10, Successful Guesses: 0, Times Inhibited: 2, Times Expanded: 0
Player 4 - Total Guesses: 10, Successful Guesses: 0, Times Inhibited: 0, Times Expanded: 2
```

## Lessons Learned

This experiment serves as a practical example of why global variables should be avoided in concurrent programming. They can lead to unpredictable behavior and make it challenging to ensure the correctness of your program. By simulating a game, we can see how goroutines interact with a shared variable and the complexity that arises from it.

## Future Enhancements

- Add more types of weapons and strategies.
- Implement a graphical interface for the game.
- Introduce more sophisticated player behaviors and strategies.

## Contributions

Contributions are welcome! Feel free to open issues or submit pull requests with improvements.

## License

This project is licensed under the MIT License.
