package defense

import (
	"fmt"
)

// DefenseStats represents the statistics of a player's defenses.
type DefenseStats struct {
	TotalDefenses         int
	SkipTurnDefenders     map[int]int
	DelayBombDefenders    map[int]int
	ConfusionBombDefenders map[int]int
}

// Defense represents a player with associated defense skills.
type Defense struct {
	DefenderId   int
	Stats        *DefenseStats
	ShieldTurns  int
	AntiDelayTurns int
	ClarityTurns int
}

// NewDefense initializes a new Defense instance.
func NewDefense() *Defense {
	return &Defense{
		DefenderId: -1,
		Stats: &DefenseStats{
			TotalDefenses:         0,
			SkipTurnDefenders:     make(map[int]int),
			DelayBombDefenders:    make(map[int]int),
			ConfusionBombDefenders: make(map[int]int),
		},
		ShieldTurns:  0,
		AntiDelayTurns: 0,
		ClarityTurns: 0,
	}
}

// ApplyShield applies a shield that protects against Skip Bomb attacks for a number of turns.
func (d *Defense) ApplyShield(defenderId, turns int) {
	d.DefenderId = defenderId
	d.ShieldTurns = turns
	d.Stats.TotalDefenses++
	d.Stats.SkipTurnDefenders[defenderId] = turns

	fmt.Printf("Player %d activated Shield for %d turns.\n", defenderId, turns)
}

// ApplyAntiDelay applies an anti-delay skill that protects against Delay Bomb attacks for a number of turns.
func (d *Defense) ApplyAntiDelay(defenderId, turns int) {
	d.DefenderId = defenderId
	d.AntiDelayTurns = turns
	d.Stats.TotalDefenses++
	d.Stats.DelayBombDefenders[defenderId] = turns

	fmt.Printf("Player %d activated Anti-Delay for %d turns.\n", defenderId, turns)
}

// ApplyClarity applies a clarity skill that protects against Confusion Bomb attacks for a number of turns.
func (d *Defense) ApplyClarity(defenderId, turns int) {
	d.DefenderId = defenderId
	d.ClarityTurns = turns
	d.Stats.TotalDefenses++
	d.Stats.ConfusionBombDefenders[defenderId] = turns

	fmt.Printf("Player %d activated Clarity for %d turns.\n", defenderId, turns)
}

// ClearDefense resets the defense state.
func (d *Defense) ClearDefense() {
	d.DefenderId = -1
	d.ShieldTurns = 0
	d.AntiDelayTurns = 0
	d.ClarityTurns = 0
}
