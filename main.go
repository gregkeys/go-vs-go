package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"

	"go_vs_go/defense"
	"go_vs_go/weapons"
)

var globalVar int

type GameOptions struct {
	NumWorkers           int
	NumPlayers           int
	MaxGuessesPerPlayer  int
	MaxNum               int
	GuessWindow          int
	WorkerDelay          time.Duration
	PlayerDelay          time.Duration
	OneWinner            bool
}

type GameStatus struct {
	globalVarLabel     *widget.Label
	playerStatusLabels []*widget.Label
	playerProgressBars []*widget.ProgressBar
}

func (gs *GameStatus) updateGlobalVarLabel(newValue int) {
	gs.globalVarLabel.SetText(fmt.Sprintf("Global Variable: %d", newValue))
}

func (gs *GameStatus) updatePlayerStatusLabel(playerID int, status string) {
	gs.playerStatusLabels[playerID].SetText(status)
}

func (gs *GameStatus) updatePlayerProgress(playerID int, progress float64) {
	gs.playerProgressBars[playerID].SetValue(progress)
}

type Game struct {
	mu                      sync.Mutex
	workerWg                sync.WaitGroup
	playerWg                sync.WaitGroup
	players                 []*Player
	done                    chan struct{}
	guessChan               chan struct{}
	status              *GameStatus
	options                 GameOptions
	once                    sync.Once
	finished                int
	finishedMu              *sync.Mutex
	playerStatusChan        chan PlayerStatusUpdate
}

type Player struct {
	id          int
	maxGuesses  int
	maxNum      int
	guessWindow int
	attacks     weapons.Attacks
	defense     defense.Defense
}

type PlayerStatusUpdate struct {
	PlayerID     int
	Status       string
	Guess        int
	WeaponEffect string
}

func (g *Game) worker(id int) {
	defer g.workerWg.Done()
	initialDelay := time.Duration(rand.Intn(1000)) * time.Millisecond
	time.Sleep(initialDelay)

	for {
		select {
		case <-g.done:
			return
		case <-time.After(g.options.WorkerDelay):
			g.mu.Lock()
			globalVar = rand.Intn(g.options.MaxNum + 1)
			g.status.updateGlobalVarLabel(globalVar)
			fmt.Printf("Worker %d changed globalVar to %d\n", id, globalVar)
			g.mu.Unlock()
		}
	}
}

func (g *Game) player(p *Player) {
	defer g.playerWg.Done()

	for i := 0; i < p.maxGuesses; i++ {
		select {
		case <-g.done:
			return
		default:
			ok, guess, weaponEffect := p.attacks.GetGuess()
			if !ok {
				g.updatePlayerStatus(p.id, "Under Attack", 0, weaponEffect, g.playerStatusChan)
				continue
			}

			g.mu.Lock()
			if guess == globalVar {
				g.updatePlayerStatus(p.id, "Guessed Correctly", guess, "None", g.playerStatusChan)
				p.attacks.GuessedCorrectly()
				fmt.Printf("Player %d guessed correctly!\n", p.id)
				g.mu.Unlock()
				g.finishGame(&g.finished, g.finishedMu, g.guessChan, &g.once, g.options.OneWinner)
				return
			}
			g.mu.Unlock()

			fmt.Printf("Player %d guessed %d\n", p.id, guess)

			playerToAttack := g.getRandomTargetPlayer(p.id, len(g.players))
			if rand.Intn(3) == 0 {
				g.players[playerToAttack].attacks.ApplyDelayBomb(p.id, 5000*time.Millisecond, &g.players[playerToAttack].defense)
			} else if rand.Intn(3) == 1 {
				g.players[playerToAttack].attacks.ApplySkipBomb(p.id, rand.Intn(5), &g.players[playerToAttack].defense)
			} else {
				g.players[playerToAttack].attacks.ApplyConfusionBomb(p.id, rand.Intn(100)+1, rand.Intn(3)+1, &g.players[playerToAttack].defense) // Apply Confusion Bomb
			}

			if rand.Intn(3) == 0 {
				p.defense.ApplyShield(p.id, 3)
			} else if rand.Intn(3) == 1 {
				p.defense.ApplyAntiDelay(p.id, 3)
			} else {
				p.defense.ApplyClarity(p.id, 3)
			}

			g.updatePlayerStatus(p.id, fmt.Sprintf("Guessed %d/%d", i+1, p.maxGuesses), guess, weaponEffect, g.playerStatusChan)
			g.status.updatePlayerProgress(p.id, float64(i+1)/float64(p.maxGuesses))
		}
	}

	fmt.Printf("Player %d used all %d guesses and didn't win.\n", p.id, p.maxGuesses)

	g.finishGame(&g.finished, g.finishedMu, g.guessChan, &g.once, g.options.OneWinner)
}

func (g *Game) getRandomTargetPlayer(currentPlayerID, numPlayers int) int {
	if numPlayers <= 1 {
		return -1
	}

	for {
		targetPlayer := rand.Intn(numPlayers)
		if targetPlayer != currentPlayerID {
			return targetPlayer
		}
	}
}

func (g *Game) finishGame(finished *int, finishedMu *sync.Mutex, guessChan chan struct{}, once *sync.Once, OneWinner bool) {
	finishedMu.Lock()
	defer finishedMu.Unlock()
	if OneWinner {
		once.Do(func() { close(guessChan) })
	}

	*finished++
	if *finished == len(g.players) {
		once.Do(func() { close(guessChan) })
	}
}

func (g *Game) updatePlayerStatus(playerID int, status string, guess int, weaponEffect string, playerStatusChan chan<- PlayerStatusUpdate) {
	playerStatusChan <- PlayerStatusUpdate{
		PlayerID:     playerID,
		Status:       status,
		Guess:        guess,
		WeaponEffect: weaponEffect,
	}
}

func NewGame(options GameOptions, gameStatus *GameStatus) *Game {
	return &Game{
		done:                  make(chan struct{}),
		options:               options,
		status: 		       gameStatus,
		once:                  sync.Once{},
		guessChan:             make(chan struct{}),
		finished:              0,
		finishedMu:            &sync.Mutex{},
		playerStatusChan:      make(chan PlayerStatusUpdate),
	}
}

func (g *Game) runGame() string {
	rand.Seed(time.Now().UnixNano())

	for i := 0; i < g.options.NumPlayers; i++ {
		player := &Player{
			id:          i,
			maxGuesses:  g.options.MaxGuessesPerPlayer,
			maxNum:      g.options.MaxNum,
			guessWindow: g.options.GuessWindow,
			attacks:     *weapons.NewAttacks(g.options.MaxNum, g.options.PlayerDelay),
			defense:     *defense.NewDefense(),
		}
		g.players = append(g.players, player)
	}

	for i := 0; i < g.options.NumWorkers; i++ {
		g.workerWg.Add(1)
		go g.worker(i)
	}

	for _, p := range g.players {
		g.playerWg.Add(1)
		go g.player(p)
	}
	go func() {
		for update := range g.playerStatusChan {
			status := fmt.Sprintf("Player %d: Status: %s, Guess: %d, Effect: %s",
				update.PlayerID, update.Status, update.Guess, update.WeaponEffect)
			g.status.updatePlayerStatusLabel(update.PlayerID, status)
		}
	}()

	<-g.guessChan
	close(g.done)
	g.workerWg.Wait()
	g.playerWg.Wait()
	close(g.playerStatusChan)

	result := "Game Over\n"
	for _, p := range g.players {
		result += fmt.Sprintf("Player %d - Total Guesses: %d, Times Inhibited: %d, Times Delayed: %d, Guessed Correctly: %t\n",
			p.id, p.attacks.Stats.TotalGuesses, len(p.attacks.Stats.SkipTurnAttackers),
			len(p.attacks.Stats.DelayBombAttackers), p.attacks.Stats.GuessedCorrectly)
	}

	return result
}

func setupUI(options GameOptions) (fyne.Window, *GameStatus, *fyne.Container) {
	myApp := app.New()
	myWindow := myApp.NewWindow("Go Guessing Game")

	globalVarLabel := widget.NewLabel("Global Variable: 0")
	playerStatusLabels := make([]*widget.Label, options.NumPlayers)
	playerProgressBars := make([]*widget.ProgressBar, options.NumPlayers)

	for i := range playerStatusLabels {
		playerStatusLabels[i] = widget.NewLabel(fmt.Sprintf("Player %d: Guessing...", i))
		playerProgressBars[i] = widget.NewProgressBar()
		playerProgressBars[i].SetValue(0)
	}

	playerStatusContainer := container.NewVBox()
	for i := range playerStatusLabels {
		playerStatusContainer.Add(container.NewVBox(
			playerStatusLabels[i],
			playerProgressBars[i],
		))
	}

	gameStatus := &GameStatus{
		globalVarLabel:     globalVarLabel,
		playerStatusLabels: playerStatusLabels,
		playerProgressBars: playerProgressBars,
	}

	statusContainer := container.NewVBox(
		playerStatusContainer,
	)

	myWindow.SetContent(container.NewVScroll(
		container.NewVBox(
			globalVarLabel,
			statusContainer,
		),
	))

	return myWindow, gameStatus, statusContainer
}

func main() {
	options := GameOptions{
		NumWorkers:           3,
		NumPlayers:           5,
		MaxGuessesPerPlayer:  10,
		MaxNum:               10,
		GuessWindow:          10,
		WorkerDelay:          3 * time.Second,
		PlayerDelay:          2 * time.Second,
		OneWinner:            false,
	}

	myWindow, gameStatus, _ := setupUI(options)
	game := NewGame(options, gameStatus)

	go func() {
		result := game.runGame()
		myWindow.SetContent(container.NewVScroll(
			container.NewVBox(
				gameStatus.globalVarLabel,
				widget.NewLabel(result),
			),
		))
	}()

	myWindow.Resize(fyne.NewSize(800, 500))
	myWindow.ShowAndRun()
}
