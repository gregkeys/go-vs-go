package main

import (
	"testing"
	"time"
)

func TestGameExecution(t *testing.T) {
	options := GameOptions{
		NumWorkers:          3,
		NumPlayers:          5,
		MaxGuessesPerPlayer: 5,
		MaxNum:              10,
		GuessWindow:         10,
		WorkerDelay:         1 * time.Second,
		PlayerDelay:         1 * time.Second,
		OneWinner:           false,
	}

	_, gameStatus, _ := setupUI(options)

	game := NewGame(options, gameStatus)

	// Start game execution in a goroutine
	go func(g *Game) {
		g.runGame()
	}(game)

	if game.finished != 0 {
		t.Errorf("Expected game to start with finished = 0, got %d", game.finished)
	}

	// Simulate game completion (for simplicity, assume it completes within reasonable time)
	time.Sleep(20 * time.Second)

	// Test game initialization
	if len(game.players) != options.NumPlayers {
		t.Errorf("Expected %d players, got %d", options.NumPlayers, len(game.players))
	}

	// Test game completion
	if game.finished != options.NumPlayers {
		t.Errorf("Expected all players to finish, got %d players finished", game.finished)
	}

	// Additional tests for player behavior, global variable updates, etc. can be added
	// as needed based on specific functionalities.

	// Clean up resources if necessary
	// close(game.done)
	// close(game.playerStatusChan)
}
