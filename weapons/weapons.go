package weapons

import (
	"fmt"
	"math/rand"
	"time"

	"go_vs_go/defense"
)

// AttackStats represents the statistics of a player's attacks.
type AttackStats struct {
	TotalGuesses       int
	SkipTurnAttackers  map[int]int
	DelayBombAttackers map[int]time.Duration
	ConfusionAttackers map[int]int
	GuessedCorrectly   bool
	MaxNum             int
}

// Attacks represents a player with associated attack statistics.
type Attacks struct {
	AttackerID      int
	Stats           *AttackStats
	TurnCount       int
	SkipTurns       int
	DelayTurn       time.Duration
	ConfusionTurns  int
	ConfusionAmount int
	OriginalDelay   time.Duration // The original delay to reset if needed
	WeaponEffect    string
}

// NewAttacks initializes a new Attacks instance with the given guess delay.
func NewAttacks(maxNum int, guessDelay time.Duration) *Attacks {
	return &Attacks{
		AttackerID: -1,
		Stats: &AttackStats{
			TotalGuesses:       0,
			SkipTurnAttackers:  make(map[int]int),
			DelayBombAttackers: make(map[int]time.Duration),
			ConfusionAttackers: make(map[int]int),
			GuessedCorrectly:   false,
			MaxNum:             maxNum,
		},
		TurnCount:     0,
		SkipTurns:     0,
		DelayTurn:     0,
		ConfusionTurns: 0,
		OriginalDelay: guessDelay,
		WeaponEffect:  "None",
	}
}

// ClearAttack resets the attack state.
func (a *Attacks) ClearAttack(effect string) {
	a.AttackerID = -1
	a.TurnCount = 0
	a.SkipTurns = 0
	a.DelayTurn = 0
	a.ConfusionTurns = 0
	a.WeaponEffect = effect
}

// CanGuess determines if the player can make a guess.
func (a *Attacks) GetGuess() (bool, int, string) {
	// Apply the original delay.
	time.Sleep(a.OriginalDelay)

	// Check if we need to skip the turn.
	if a.SkipTurns > 0 && a.TurnCount < a.SkipTurns {
		a.TurnCount++
		return false, -1,  a.WeaponEffect
	}

	// Increment the total guesses.
	a.Stats.TotalGuesses++
	guess := rand.Intn(a.Stats.MaxNum + 1)

	// Apply the delay bomb effect if active.
	if a.DelayTurn > 0 {
		time.Sleep(a.DelayTurn)
		a.ClearAttack("Delay Bomb")
		return true, guess, a.WeaponEffect
	}

	// Apply the confusion bomb effect if active.
	if a.ConfusionTurns > 0 {
		a.ConfusionTurns--
		guess = rand.Intn(a.Stats.MaxNum + a.ConfusionAmount)
		return true, guess, a.WeaponEffect
	}

	a.ClearAttack("None")
	return true, guess, a.WeaponEffect
}

// GuessedCorrectly marks the player as having guessed correctly.
func (a *Attacks) GuessedCorrectly() {
	a.Stats.GuessedCorrectly = true
}

// ApplySkipBomb applies a skip bomb to a player.
func (a *Attacks) ApplySkipBomb(attackerID, skipTurns int, defender *defense.Defense) {
	if defender.ShieldTurns > 0 {
		fmt.Printf("Player %d's Shield absorbed the Skip Bomb.\n", defender.DefenderId)
		return
	}

	if a.AttackerID != -1 {
		fmt.Printf("Player is currently under attack by Player %d for %d turns.\n", a.AttackerID, a.SkipTurns)
		return
	}
	a.AttackerID = attackerID
	a.SkipTurns = skipTurns
	a.Stats.SkipTurnAttackers[attackerID] = skipTurns
	a.WeaponEffect = "Skip Bomb"

	fmt.Printf("Player %d used a Skip Bomb inhibiting for %d turns.\n", attackerID, skipTurns)
}

// ApplyDelayBomb applies a delay bomb to a player, increasing their delay by a specified duration.
func (a *Attacks) ApplyDelayBomb(attackerID int, delay time.Duration, defender *defense.Defense) {
	if defender.AntiDelayTurns > 0 {
		fmt.Printf("Player %d's Anti-Delay absorbed the Delay Bomb.\n", defender.DefenderId)
		return
	}

	if a.AttackerID != -1 {
		fmt.Printf("Player is currently under attack by Player %d with a %s.\n", a.AttackerID, a.WeaponEffect)
		return
	}
	a.AttackerID = attackerID
	a.DelayTurn = delay
	a.Stats.DelayBombAttackers[attackerID] = delay
	a.WeaponEffect = "Delay Bomb"

	fmt.Printf("Player %d used a Delay Bomb increasing their guess delay by %v\n", attackerID, delay)
}

// ApplyConfusionBomb applies a confusion bomb to a player, causing them to make random guesses for a specified number of turns.
func (a *Attacks) ApplyConfusionBomb(attackerID, ConfusionAmount, confusionTurns int, defender *defense.Defense) {
	if defender.ClarityTurns > 0 {
		fmt.Printf("Player %d's Clarity absorbed the Confusion Bomb.\n", defender.DefenderId)
		return
	}

	if a.AttackerID != -1 {
		fmt.Printf("Player is currently under attack by Player %d with a %s.\n", a.AttackerID, a.WeaponEffect)
		return
	}
	a.AttackerID = attackerID
	a.ConfusionTurns = confusionTurns
	a.ConfusionAmount = ConfusionAmount
	a.Stats.ConfusionAttackers[attackerID] = confusionTurns
	a.WeaponEffect = "Confusion Bomb"

	fmt.Printf("Player %d used a Confusion Bomb causing random guesses for %d turns.\n", attackerID, confusionTurns)
}
